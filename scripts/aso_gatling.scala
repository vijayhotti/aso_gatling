
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class aso_gatling extends Simulation {

  val httpProtocol = http
    .baseURL("http://detectportal.firefox.com")
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.(t|o)tf""", """.*\.png"""), WhiteList())
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0")

  val headers_0 = Map("Pragma" -> "no-cache")

  val headers_3 = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "DNT" -> "1",
    "Upgrade-Insecure-Requests" -> "1")

//  val Users = System.getProperty("Users").toInt
//  val Duration = System.getProperty("Duration").toInt
//  val Product = System.getProperty("Product")
//  val Category = System.getProperty("Category")
//  val Seo = System.getProperty("Seo")
//  val Inventory = System.getProperty("Inventory")
//  val Search = System.getProperty("Search")


    val Users = 1
    val Duration = 60
    val Product = "true"
    val Category = "true"
    //val Seo = "true"
    val Variant = "true"
    val Inventory = "true"
    val Search = "true"
    val Session = "true"
    val Store = "true"

  val hp_feed = csv("hp_feeder.csv").random
  val product_feeder = csv("product_feeder.csv").random
  //val seo_feeder = csv("seo_feeder.csv").random
  val variant_feeder = csv("variant_feeder.csv").random
  val inventory_feeder = csv("inventory_feeder.csv").random
  val search_feeder = csv("search_feeder.csv").random
  val cart_feeder = csv("cart_feeder.csv").random
  val store_feeder = csv("store_feeder.csv").random


  val uri1 = "http://35.202.244.154/api/"

  //val uri2 = "http://35.202.235.124/api/"

  object ProductAPI {
    val Carousel = feed(product_feeder).exec(http("Prdt_Carousel")
      .get(uri1 + "productinfo?productIds=${prod}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val APIByProduct = feed(product_feeder).exec(http("Prdt_PDP")
      .get(uri1 + "productinfo/${prod}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val FacetCategory = feed(product_feeder).exec(http("Prdt_FacetCategory")
      .get(uri1 + "category/${cat}/products?facets=true").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val PDPAndInventory = feed(product_feeder).exec(http("Prdt_PDPAndInvt")
      .get(uri1 + "product/${prod}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object CategoryAPI {
    val MultiCategory = feed(hp_feed).exec(http("Categ_MultiCategory")
      .get(uri1 + "category?categoryIds=${category}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val FacetDetails = feed(hp_feed).exec(http("Categ_FacetDetails")
      .get(uri1 + "category/${category}/facets").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object VariantAPI {
    val ProductName = feed(variant_feeder).exec(http("Variant_ProdSeoName")
      .get(uri1 + "variant?prodSeoName=${ProdName}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val CategoryName = feed(variant_feeder).exec(http("Variant_CatSeoName")
      .get(uri1 + "variant?catSeoName=${CategName}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val ProductID = feed(variant_feeder).exec(http("Variant_SeoPrdtID")
      .get(uri1 + "variant/seo?productId=${ProdID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val CategoryID = feed(variant_feeder).exec(http("Variant_SeoCategID")
      .get(uri1 + "variant/seo?categoryId=${CategID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object InventoryAPI {
    val OnlineAndStore = feed(inventory_feeder).exec(http("Invt_OnlineAndStore")
      .get(uri1 + "inventory?productId=${ProductID}&storeId=${StoreID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val Online = feed(inventory_feeder).exec(http("Invt_Online")
      .get(uri1 + "inventory/${ProductID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val Store = feed(inventory_feeder).exec(http("Invt_Store")
      .get(uri1 + "inventory/store/${StoreID}?productId=${ProductID}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val Notify = feed(inventory_feeder).exec(http("Invt_Notify")
      .get(uri1 + "inventory/notify").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object SearchAPI {
    val AutoSuggest = feed(search_feeder).exec(http("Search_AutoSuggest")
      .get(uri1 + "search/autosuggest/${SearchTerm}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val SiteContent = feed(search_feeder).exec(http("Search_SiteContent")
      .get(uri1 + "search/sitecontent").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val SiteSearch = feed(search_feeder).exec(http("Search_SiteSearch")
      .get(uri1 + "search/sitesearch/${SearchTerm}?orderBy=&pageSize=48&pageNumber=1&facet=").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val FacetSearch = feed(search_feeder).exec(http("Search_FacetSearch")
      .get(uri1 + "search?facets=${Facet}&orderBy=1&pageSize=5&categoryId=${Category}&pageNumber=1").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val GetSearch = feed(search_feeder).exec(http("Search_GetSearch")
      .get(uri1 + "getSearch").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
    val ProductSuggession = feed(search_feeder).exec(http("Srch_PrdtSuggession")
      .get(uri1 + "search/productSuggestions/${SearchTerm}").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object SessionAPI {
    val Initiate = exec(http("Session_Initiate")
      .get(uri1 + "session").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object CartAPI {
    val GetDetails = feed(cart_feeder).exec(http("Cart_GetDetails")
      .get(uri1 + "cart").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  object StoreAPI {
    val GetDetails = feed(store_feeder).exec(http("Store_GetDetails")
      .get(uri1 + "stores?lat=${latitude}&lon=${longitude}&storeId=").check(status.is(200))
      .headers(headers_3)
    )
      .pause(3)
  }

  val scn1 = scenario("Product")
    .forever {
      exec(flushHttpCache)
        .doIf(Product == "true") {
          exec(
            ProductAPI.Carousel,
            ProductAPI.APIByProduct,
            ProductAPI.FacetCategory,
            ProductAPI.PDPAndInventory
          )
        }
    }


  val scn2 = scenario("Category")
    .forever {
      exec(flushHttpCache)
        .doIf(Category == "true") {
          exec(
            CategoryAPI.MultiCategory,
            CategoryAPI.FacetDetails
          )
        }
    }

//  val scn3 = scenario("Seo")
//    .forever {
//      exec(flushHttpCache)
//        .doIf(Seo == "true") {
//          exec(
//            SeoAPI.ProductName,
//            SeoAPI.CategoryName
//          )
//        }
//    }

  val scn3 = scenario("Variant")
    .forever {
      exec(flushHttpCache)
        .doIf(Variant == "true") {
          exec(
            VariantAPI.ProductName,
            VariantAPI.CategoryName,
            VariantAPI.ProductID,
            VariantAPI.CategoryID
          )
        }
    }


  val scn4 = scenario("Inventory")
    .forever {
      exec(flushHttpCache)
        .doIf(Inventory == "true") {
          exec(
            InventoryAPI.OnlineAndStore,
            InventoryAPI.Online,
            InventoryAPI.Store,
            InventoryAPI.Notify
          )
        }
    }

  val scn5 = scenario("Search")
    .forever {
      exec(flushHttpCache)
        .doIf(Search == "true") {
          exec(
            SearchAPI.AutoSuggest,
            SearchAPI.SiteContent,
            SearchAPI.SiteSearch,
            SearchAPI.FacetSearch,
            SearchAPI.GetSearch,
            SearchAPI.ProductSuggession
          )
        }
    }

  val scn6 = scenario("Session")
    .forever {
      exec(flushHttpCache)
        .doIf(Session == "true") {
          exec(
            SessionAPI.Initiate
          )
        }
    }

  val scn7 = scenario("Store")
    .forever {
      exec(flushHttpCache)
        .doIf(Store == "true") {
          exec(
            StoreAPI.GetDetails
          )
        }
    }
  // setUp(scn.inject(rampUsers(Users) over (5 seconds))
  //					).protocols(httpProtocol).maxDuration(Duration seconds)
  setUp(scn1.inject(rampUsers(Users) over (5 seconds)),
    scn2.inject(rampUsers(Users) over (5 seconds)),
    scn3.inject(rampUsers(Users) over (5 seconds)),
    scn4.inject(rampUsers(Users) over (5 seconds)),
    scn5.inject(rampUsers(Users) over (5 seconds)),
    scn6.inject(rampUsers(Users) over (5 seconds)),
    scn7.inject(rampUsers(Users) over (5 seconds))
  ).protocols(httpProtocol).maxDuration(Duration seconds)
}